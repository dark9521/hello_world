import java.text.DecimalFormat;
import java.util.Arrays;

//Увеличивает элементы одномерного массива на 10%, а затем сортирует их по убыванию методом "пузырька"
public class mas {

    public static void main(String[] args) {

        System.out.println("Nachalniy massiv:");
        double[] massiv = new double [7];
        for (int i = 0; i < massiv.length; i++) {
            massiv[i] = (double) Math.round((Math.random() * 30) - 10) ;
            DecimalFormat df = new DecimalFormat("###.##");
            System.out.print(df.format(massiv[i]));
            System.out.print("  ");
        }
        System.out.println();
        System.out.println("Yvelechenie chisel na 10%:");
        for (int i = 0; i < massiv.length; i++) {
            massiv[i] *= 1.1;
            DecimalFormat df = new DecimalFormat("###.##");
            System.out.print(df.format(massiv[i]));
            System.out.print("  ");
        }
        System.out.println();
        System.out.println("Otsortirovanniy massiv:");
        for (int i = massiv.length - 1; i > 0; i--) {
            for (int j = 0 ; j < i; j++) {
                if (massiv[j] < massiv[j+1]) {
                    double tmp = massiv[j];
                    massiv[j] = massiv[j + 1];
                    massiv[j + 1] = tmp;
                }
            }
        }
        DecimalFormat df = new DecimalFormat("###.##");
        for (int i = 0; i < massiv.length; i++) {
            System.out.print(df.format(massiv[i]));
            System.out.print(" ");
        }
    }
}