import java.util.Scanner;

public class zadanie_2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float зарплата_в_час;
        do {
            System.out.println("Работник не может получать меньше 8 долларов в час");
            System.out.println("Введите зарплату в час");
            зарплата_в_час = in.nextFloat();
        } while (зарплата_в_час < 8);

        float рабочие_часы;
        do {
            System.out.println("Работник не может работать больше, чем 60 часов в неделю");
            System.out.println("Введите число рабочих часов");
            рабочие_часы = in.nextFloat();
        } while (рабочие_часы > 60 || рабочие_часы < 0);

        System.out.println("Зарплата в неделю: $" + method(зарплата_в_час, рабочие_часы));
    }

    public static float method (float зарплата_в_час, float рабочие_часы) {
        float зарплата_в_неделю;
        if (рабочие_часы > 40) {
            рабочие_часы -= 40;
            зарплата_в_неделю = 40 * зарплата_в_час + рабочие_часы * 1.5f * зарплата_в_час;
        } else {
            зарплата_в_неделю = рабочие_часы * зарплата_в_час;
        }
        return зарплата_в_неделю;
    }
}
